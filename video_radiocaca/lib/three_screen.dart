import 'dart:async';
import 'dart:math';

import 'package:flutter/material.dart';

class PageThree extends StatefulWidget {
  @override
  _PageThreeState createState() => _PageThreeState();
}

class _PageThreeState extends State<PageThree> {

  bool check = true;
  double marginLeft = 0;
  double marginTop = 0;
  bool thuan_chieu_kdh = true;
  var _timer;
  MaterialColor get randomColor => Colors.primaries[Random().nextInt(17)];
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _timer = Timer.periodic(Duration(seconds: 1), (timer) {

      if (mounted) {
        if(thuan_chieu_kdh){
          if (marginLeft == (MediaQuery.of(context).size.width - 50)) {
            marginLeft = 0;
            marginTop += 80;
            if(marginTop >= MediaQuery.of(context).size.height - 130){
              thuan_chieu_kdh = false;
            }

          } else {
            marginLeft = (MediaQuery.of(context).size.width - 50);
            marginTop += 50;
            if(marginTop >= MediaQuery.of(context).size.height - 130){
              thuan_chieu_kdh = false;
            }
          }
        }else{
          if (marginLeft == (MediaQuery.of(context).size.width - 50)) {
            marginLeft = 0;
            marginTop -= 100;
            if(marginTop <= 0){
              thuan_chieu_kdh = true;
            }

          } else {
            marginLeft = (MediaQuery.of(context).size.width - 50);
            marginTop -= 100;
            if(marginTop <= 0){
              thuan_chieu_kdh = true;
            }
          }
        }


        setState(() {

        });
      }

    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _buildAppBar(MediaQuery.of(context).size.width - 50,MediaQuery.of(context).size.height - 130),
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        color: Colors.blueGrey,
        alignment: Alignment.topLeft,
        child: SafeArea(
          child: Stack(
            children: [
              AnimatedPositioned(
                left: check
                    ?  marginLeft
                    : 0,
                top: check?  marginTop : 0,
                duration: const Duration(seconds: 1),
                child: Container(
                  width: 50,
                  height: 50,
                  color: randomColor,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  _buildAppBar(width, height) {
    return AppBar(
      title: Text('Back'),
      actions: [
        FlatButton(
          child: Text('Start'),
          onPressed: (){
            setState(() {
              marginLeft = 0;
              marginTop = 0;

              //check = !check;

              // if(marginLeft == 0){
              //   marginLeft = maxLeft;
              //   marginTop= maxTop - 100;
              // }
              // if(marginLeft == maxLeft){
              //   marginLeft = 0;
              //   marginTop = maxTop;
              // }

              // if(marginLeft >= maxLeft){
              //   marginLeft = maxLeft;
              //   marginTop = maxTop;
              //
              //
              // }

            });
          },
        ),
      ],
    );
  }


}
