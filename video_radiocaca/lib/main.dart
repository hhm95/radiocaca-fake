import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter_svg/svg.dart';
import 'package:video_player/video_player.dart';
import 'package:video_radiocaca/second_screen.dart';

import 'components/partner.dart';

void main() {
  runApp(MaterialApp(debugShowCheckedModeBanner: false, home: MyApp()));
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  late VideoPlayerController _controller;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _controller = VideoPlayerController.network(
        'https://racawebsource.s3.us-east-2.amazonaws.com/assets/media/banner_video.mp4')
      ..initialize().then((_) {
        _controller.play();
        _controller.setLooping(true);
        setState(() {});
      });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        body: SingleChildScrollView(
          scrollDirection: Axis.vertical,
          child: SafeArea(
            top: true,
            child: Column(
              children: [
                Container(
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height,
                  child: Stack(
                    children: [
                      SizedBox.expand(
                        child: FittedBox(
                          fit: BoxFit.cover,
                          child: SizedBox(
                            width: _controller.value.size.width,
                            height: _controller.value.size.height,
                            child: VideoPlayer(_controller),
                          ),
                        ),
                      ),
                      Column(
                        children: [
                          Align(
                            alignment: Alignment.topCenter,
                            child: Container(
                              margin: EdgeInsets.only(top: 10),
                              child: SvgPicture.network(
                                'https://racawebsource.s3.us-east-2.amazonaws.com/assets/logo.svg',
                                // placeholderBuilder: (BuildContext context) =>
                                //     Container(
                                //         padding: const EdgeInsets.all(30.0),
                                //         child: const CircularProgressIndicator()),
                              ),
                              // child: Image.network(
                              //     ''),
                            ),
                          ),
                          Align(
                            alignment: Alignment.topCenter,
                            child: Container(
                              margin: const EdgeInsets.all(15.0),
                              decoration: BoxDecoration(
                                border:
                                    Border.all(color: Colors.white, width: 2),
                                borderRadius: BorderRadius.circular(10),
                              ),
                              child: FlatButton(
                                onPressed: goOtherPage,
                                child: Text(
                                  'Connect Wallet',
                                  style: TextStyle(color: Colors.white),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                Container(
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height,
                  decoration: const BoxDecoration(
                    image: DecorationImage(
                      fit: BoxFit.cover,
                      image: NetworkImage(
                          'https://racawebsource.s3.us-east-2.amazonaws.com/assets/bg.jpg'),
                    ),
                  ),
                  alignment: Alignment.topLeft,
                  child: Container(
                    margin: EdgeInsets.fromLTRB(20, 100, 0, 0),
                    width: 280,
                    child: SizedBox(
                      child: Column(
                        children: const [
                          Align(
                            alignment: Alignment.centerLeft,
                            child: Text(
                              'Radio Caca --',
                              style:
                                  TextStyle(color: Colors.white, fontSize: 30),
                            ),
                          ),
                          SizedBox(
                            height: 30,
                          ),
                          Text(
                            'Radio Caca is the exclusive manager of Maye Musk Mystery Box (MPB) NFT and DeFi+GameFi vehicle for The USM Metaverse.',
                            style: TextStyle(color: Colors.white, fontSize: 18),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                Container(
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height + 250,
                  color: Colors.black,
                  alignment: Alignment.center,
                  child: Container(
                    margin: EdgeInsets.fromLTRB(20, 70, 0, 0),
                    child: SizedBox(
                      child: Column(
                        children: [
                          const Align(
                              alignment: Alignment.center,
                              child: Text(
                                'PARTNERSHIP',
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 24,
                                    fontWeight: FontWeight.bold),
                              ),
                          ),
                           SizedBox(
                            height: 60,
                          ),

                                OnePartner(
                                    'https://racawebsource.s3.us-east-2.amazonaws.com/assets/logo_bsc.png',
                                    'https://www.binance.org'),
                                OnePartner(
                                    'https://racawebsource.s3.us-east-2.amazonaws.com/assets/logo_binancenft.png',
                                    'https://www.binance.com/en/nft'),
                                OnePartner(
                                    'https://racawebsource.s3.us-east-2.amazonaws.com/assets/logo_stanford.png',
                                    'https://ai.stanford.edu'),
                                OnePartner(
                                    'https://racawebsource.s3.us-east-2.amazonaws.com/assets/logo_stanford.png',
                                    'https://ai.google/education'),
                          SizedBox(
                            height: 60,
                          ),
                          Column(
                              children: [
                                Align(
                                  alignment: Alignment.center,
                                  child: Text(
                                    '@2021 by RadioCaca.COM',
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 14,
                                        ),
                                  ),
                                ),
                                Align(
                                  alignment: Alignment.center,
                                  child: Text(
                                    'Contact Us : team@radiocaca.com',
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 14,
                                        ),
                                  ),
                                ),
                                Container(
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: [
                                      SizedBox(width: 50,),
                                      Align(
                                        alignment: Alignment.topCenter,
                                        child: Container(
                                          margin: EdgeInsets.only(top: 10),
                                          child: SvgPicture.network(
                                            'https://racawebsource.s3.us-east-2.amazonaws.com/assets/telegram.svg',
                                            // placeholderBuilder: (BuildContext context) =>
                                            //     Container(
                                            //         padding: const EdgeInsets.all(30.0),
                                            //         child: const CircularProgressIndicator()),
                                          ),
                                          // child: Image.network(
                                          //     ''),
                                        ),
                                      ),
                                      Align(
                                        alignment: Alignment.topCenter,
                                        child: Container(
                                          margin: EdgeInsets.only(top: 10),
                                          child: SvgPicture.network(
                                            'https://racawebsource.s3.us-east-2.amazonaws.com/assets/twitter.svg',
                                            // placeholderBuilder: (BuildContext context) =>
                                            //     Container(
                                            //         padding: const EdgeInsets.all(30.0),
                                            //         child: const CircularProgressIndicator()),
                                          ),
                                          // child: Image.network(
                                          //     ''),
                                        ),
                                      ),
                                      Align(
                                        alignment: Alignment.topCenter,
                                        child: Container(
                                          margin: EdgeInsets.only(top: 10),
                                          child: SvgPicture.network(
                                            'https://racawebsource.s3.us-east-2.amazonaws.com/assets/medium.svg',
                                            // placeholderBuilder: (BuildContext context) =>
                                            //     Container(
                                            //         padding: const EdgeInsets.all(30.0),
                                            //         child: const CircularProgressIndicator()),
                                          ),
                                          // child: Image.network(
                                          //     ''),
                                        ),
                                      ),
                                      SizedBox(width: 50,),
                                    ],
                                  ),
                                ),
                                SizedBox(
                                  height: 15,
                                ),
                              ]

                          ),
                        ],
                      ),
                    ),
                  ),
                ),

              ],
            ),
          ),
        ),
      ),
    );
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    _controller.dispose();
  }

  goOtherPage() {
    double randomNumber = Random().nextDouble() * 100;
    print(randomNumber);

    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => SecondScreen()),
    );
  }
}
