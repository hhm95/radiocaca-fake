import 'package:flutter/material.dart';

class OnePartner extends StatefulWidget {
  final String img;
  final String link;

  // OnePartner(this.img, this.link, {Key? key}) : super(key: key);
  const OnePartner(this.img, this.link);

  @override
  _OnePartnerState createState() => _OnePartnerState();
}

class _OnePartnerState extends State<OnePartner> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.fromLTRB(70, 10, 70, 10),
      width: 274,
      height: 135,
      decoration: BoxDecoration(
        border: Border.all(color: Colors.white, width: 0.5),
        borderRadius: BorderRadius.circular(10),
      ),
      child: Column(
        children: [
          ClipRRect(
            child: FittedBox(
              fit: BoxFit.fitWidth,
              alignment: Alignment.bottomCenter,
              child: ConstrainedBox(
                constraints: BoxConstraints(minWidth: 1, minHeight: 1), // here
                child: Image.network(widget.img
                ),
              ),
            ),
          ),
          FlatButton(
            onPressed: () {},
            child: Text(
              widget.link,
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 11,
                  decoration: TextDecoration.underline),
            ),
          )
        ],
      ),
    );
  }
}
