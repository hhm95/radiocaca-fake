import 'dart:math';

import 'package:flutter/material.dart';
import 'package:video_radiocaca/three_screen.dart';

class SecondScreen extends StatefulWidget {
  @override
  _SecondScreenState createState() => _SecondScreenState();
}

class _SecondScreenState extends State<SecondScreen> {

  MaterialColor get randomColor => Colors.primaries[Random().nextInt(17)];
  // Random get randomNumber => Random().nextDouble() * 100 + Random().nextInt(100);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _buildAppBar(),
      body: Container(
        color: Colors.grey,
        child: Stack(
          children: [
            SquareContainer(randomNumber(),randomColor),
            SquareContainer(randomNumber(),randomColor),
            SquareContainer(randomNumber(),randomColor),
            SquareContainer(randomNumber(),randomColor),
            SquareContainer(randomNumber(),randomColor),
            SquareContainer(randomNumber(),randomColor),
            SquareContainer(randomNumber(),randomColor),
            SquareContainer(randomNumber(),randomColor),
            SquareContainer(randomNumber(),randomColor),
            SquareContainer(randomNumber(),randomColor),
            SquareContainer(randomNumber(),randomColor),
            SquareContainer(randomNumber(),randomColor),
            SquareContainer(randomNumber(),randomColor),
            SquareContainer(randomNumber(),randomColor),
            SquareContainer(randomNumber(),randomColor),
            SquareContainer(randomNumber(),randomColor),
            SquareContainer(randomNumber(),randomColor),
            SquareContainer(randomNumber(),randomColor),
            SquareContainer(randomNumber(),randomColor),
            SquareContainer(randomNumber(),randomColor),
            SquareContainer(randomNumber(),randomColor),
            SquareContainer(randomNumber(),randomColor),
            SquareContainer(randomNumber(),randomColor),
            SquareContainer(randomNumber(),randomColor),
            SquareContainer(randomNumber(),randomColor),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.refresh_outlined),
        onPressed: refreshPage,
      ),
    );
  }

  _buildAppBar() {
    return AppBar(
      // title: Text('Second Page'),
      actions: [
        Row(
          children: [
            Text('Next Page',style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold,),),
            IconButton(
              onPressed: nextPage,
              icon: Icon(Icons.navigate_next_outlined),
            ),
          ],
        ),
      ],
    );
  }

  SquareContainer( double randomNumber, randomColor){
    return Positioned(
      top: Random().nextDouble() * 600 + Random().nextInt(20),
      left: Random().nextDouble() * 300 + Random().nextInt(50),
      child: Container(
        width: randomNumber,
        height: randomNumber,
        color: randomColor,
      ),
    );
  }

  double randomNumber(){
    // Random get randomNumber => Random().nextDouble() * 100 + Random().nextInt(100);
    double randomNumber = Random().nextDouble() * 150;
    return randomNumber;
  }

  refreshPage(){
    setState(() {

    });
    // Navigator.push(
    //   context,
    //   MaterialPageRoute(builder: (context) => SecondScreen()),
    // );
  }

  nextPage(){
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => PageThree()),
    );
  }
}
